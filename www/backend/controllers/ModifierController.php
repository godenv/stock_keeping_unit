<?php

namespace backend\controllers;

use Yii;
use common\models\Modifier;
use common\models\Product;
use common\services\ModifierService;
use yii\helpers\ArrayHelper;

class ModifierController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $step = 0;
        $modifier = new Modifier();
        $modifier->load(Yii::$app->request->post());
        if ($modifier->product_id) {
            if ($modifier->count && $modifier->validate()) {
                $model = ModifierService::searchByModel($modifier);
                $model->load(Yii::$app->request->post());
                if ($model->save()) {
                    return $this->render('success');
                }
            }
            $modifier = ModifierService::searchByModel($modifier);
            $step = 1;
        }

        $productIdList = ArrayHelper::map(
            ArrayHelper::toArray(
                Product::find()->all(), [
                    'common\models\Product' => ['id', 'name']
                ]
            ),
            'id',
            'name'
        );
        return $this->render('index', [
            'modifier' => $modifier,
            'productIdList' => $productIdList,
            'values' => ModifierService::VALUES,
            'step' => $step
        ]);
    }
}
