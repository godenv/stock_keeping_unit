<?php
    /**
     * @var $this yii\web\View
     * @var $modifier common\models\Modifier
     * @var $productIdList array
     * @var $values array
     * @var $step int
     */

    use yii\web\View;
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    use yii\helpers\Url;
    use kartik\depdrop\DepDrop;

    $form = ActiveForm::begin();
    echo $form->field($modifier, 'id')->hiddenInput()->label(false);
?>


<div class="row">
    <div class="col-md-2">
        <?= $form->field($modifier, 'product_id')->dropDownList($productIdList, ['id'=>'product_id']);?>
    </div>
    <div class="col-md-1">
        <?= $form->field($modifier, 'wight')->dropDownList($values, ['id'=>'width']);?>
    </div>
    <div class="col-md-1">
        <?= $form->field($modifier, 'length')->dropDownList($values, ['id'=>'length']);?>
    </div>
    <div class="col-md-1">
        <?= $form->field($modifier, 'height')->dropDownList($values, ['id'=>'height']);?>
    </div>
    <div class="col-md-1">

    </div>


    <?php if ($step) : ?>
        <div class="col-md-2">
            <?= $form->field($modifier, 'code')->input('text');?>
        </div>
        <div class="col-md-2">
            <?= $form->field($modifier, 'price')->input('text');?>
        </div>
        <div class="col-md-2">
            <?= $form->field($modifier, 'count')->input('text');?>
        </div>
    <?php endif; ?>
</div>

<div class="row">
    <?php if ($step) : ?>
        <div class="col-md-1 col-md-offset-6">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary submit']) ?>
        </div>
        <div class="col-md-1">
            <a href="<?= Url::toRoute('modifier/index');?>" class="btn btn-default">Отмена</a>
        </div>
    <?php else : ?>
        <div class="col-md-1 col-md-offset-4">
            <?= Html::submitButton('Найти', ['class' => 'btn btn-primary submit']) ?>
        </div>
    <?php endif; ?>
</div>

<?php ActiveForm::end() ?>

