<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%modifier}}".
 *
 * @property int $id
 * @property int $product_id
 * @property string $code
 * @property int $wight
 * @property int $length
 * @property int $height
 * @property int $price
 * @property int $count
 *
 * @property Product $product
 */
class Modifier extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%modifier}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'code', 'wight', 'length', 'height', 'price', 'count'], 'required'],
            [['product_id', 'wight', 'length', 'height', 'price', 'count'], 'integer'],
            [['code'], 'string', 'max' => 32],
            [['code'], 'unique'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'code' => 'Code',
            'wight' => 'Wight',
            'length' => 'Length',
            'height' => 'Height',
            'price' => 'Price',
            'count' => 'Count',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
