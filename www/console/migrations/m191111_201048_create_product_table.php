<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product}}`.
 */
class m191111_201048_create_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(32)->notNull()
        ]);

        $this->insert('{{%product}}', [
            'name' => 'товар A'
        ]);
        $this->insert('{{%product}}', [
            'name' => 'товар B'
        ]);
        $this->insert('{{%product}}', [
            'name' => 'товар C'
        ]);
        $this->insert('{{%product}}', [
            'name' => 'товар D'
        ]);
        $this->insert('{{%product}}', [
            'name' => 'товар E'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%product}}', ['name' => 'товар A']);
        $this->delete('{{%product}}', ['name' => 'товар B']);
        $this->delete('{{%product}}', ['name' => 'товар C']);
        $this->delete('{{%product}}', ['name' => 'товар D']);
        $this->delete('{{%product}}', ['name' => 'товар E']);
        $this->dropTable('{{%product}}');
    }
}
