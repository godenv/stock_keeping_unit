<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'Админка';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Админка</h1>
        <p> управления торговыми предложениями</p>
        <p><a class="btn btn-lg btn-success" href="<?= Url::toRoute('modifier/index');?>">Начать редактирование предложений</a></p>
    </div>

</div>
