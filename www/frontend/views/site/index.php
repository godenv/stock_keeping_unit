<?php

/**
 * @var $this yii\web\View
 * @var $products \common\models\Product[]
 * @var $modifiers \common\models\Modifier[]
 */

use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use yii\grid\GridView;

$this->title = 'Товары';
?>
<div class="site-index">

    <h2>Товары</h2>
    <?= GridView::widget([
        'dataProvider' => $products,
    ]);?>

    <h2>Торговые предложения</h2>
    <?= GridView::widget([
        'dataProvider' => $modifiers,
    ]);?>

</div>
