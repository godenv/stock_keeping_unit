<?php

namespace common\services;

use common\models\Modifier;

class ModifierService
{
    const VALUES = [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5];

    /**
     * @param Modifier $modifier
     * @return Modifier
     */
    public static function searchByModel(Modifier $modifier)
    {
        $search = [];
        if ($modifier->product_id && $modifier->wight && $modifier->height && $modifier->length) {
            $search['product_id'] = $modifier->product_id;
            $search['wight'] = $modifier->wight;
            $search['length'] = $modifier->length;
            $search['height'] = $modifier->height;
        }
        if (empty($search)) {
            return $modifier;
        }

        $model = Modifier::findOne($search);
        if (empty($model)) {
            return $modifier;
        }

        return $model;
    }
}
