<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%modifer}}`.
 */
class m191111_201718_create_modifier_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%modifier}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'code' => $this->string(32)->notNull(),
            'wight' => $this->tinyInteger(1)->notNull(),
            'length' => $this->tinyInteger(1)->notNull(),
            'height' => $this->tinyInteger(1)->notNull(),
            'price' => $this->integer()->notNull(),
            'count' => $this->integer()->notNull()
        ]);

        $this->createIndex(
            'idx-modifier-product_id',
            '{{%modifier}}',
            'product_id'
        );

        $this->addForeignKey(
            'fk-modifier-author_id',
            '{{%modifier}}',
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-modifier-author_id',
            '{{%modifier}}'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-modifier-product_id',
            '{{%modifier}}'
        );
        $this->dropTable('{{%modifier}}');
    }
}
